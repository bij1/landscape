from diagrams import Diagram, Cluster, Edge
from diagrams.custom import Custom
from diagrams.saas.chat import Discord
from diagrams.saas.filesharing import Nextcloud
from diagrams.saas.social import Facebook, Twitter
from diagrams.programming.language import Python, Nodejs, Php
from diagrams.programming.framework import Rails
from diagrams.elastic.elasticsearch import Beats, Elasticsearch, Kibana, Logstash

wordpress = "./resources/wordpress.png"
wings = "./resources/wings.png"
trello = "./resources/trello.png"
gitlab_logo = "./resources/gitlab.png"
github_logo = "./resources/github.svg"
mailchimp = "./resources/mailchimp.png"
nginx = "./resources/nginx.png"
www = "./resources/www.png"
instagram = "./resources/instagram.png"
tiktok_logo = "./resources/tiktok.svg"
youtube_logo = "./resources/youtube.png"
linkedin_logo = "./resources/linkedin.svg"
heroku = "./resources/heroku.png"
transip_logo = "./resources/transip.png"
greenhost_logo = "./resources/greenhost.png"
email_logo = "./resources/email.png"

with Diagram("BIJ1 IT landscape"):

    with Cluster("Socials"):

        with Cluster("BIJ1 socials landelijk"):
            fb = Facebook("facebook.com/www.bij1.org")
            tw = Twitter("twitter.com/PolitiekBIJ1")
            insta = Custom("instagram.com/politiek_bij1", instagram)
            yt = Custom("youtube.com/c/BIJ1Politiek", youtube_logo)
            vimeo = Custom("vimeo.com/bij1", "./resources/vimeo.svg")
            linkedin = Custom("linkedin.com/company/bij1", linkedin_logo)

        with Cluster("BIJ1 local socials"):
            local_fb = Facebook("Local FBs")
            local_tw = Twitter("Local Twitters")
            local_insta = Custom("Local Instagrams", instagram)
            local_yt = Custom("Local Youtubes", youtube_logo)
            local_li = Custom("Local LinkedIns", linkedin_logo)

        with Cluster("Radicaal socials"):
            fb_radicaal = Facebook("facebook.com/pjo.radicaal")
            tw_radicaal = Twitter("twitter.com/pjo_radicaal")
            insta_radicaal = Custom("instagram.com/pjo_radicaal", instagram)
            tiktok_radicaal = Custom("tiktok.com/@pjo_radicaal", tiktok_logo)

    with Cluster("Hosting"):
        transip = Custom("TransIP BIJ1", transip_logo)
        transip_kennis = Custom("TransIP Audre Lorde", transip_logo)
        transip_intl = Custom("TransIP Clemencia Redmond", transip_logo)
        greenhost = Custom("GreenHost BIJ1", greenhost_logo)
        greenhost_pjo = Custom("GreenHost Radicaal", greenhost_logo)
        hetzner = Custom("Hetzner", "./resources/hetzner.png")
        wpengine = Custom("WpEngine", "./resources/wpengine.jpg")
        servercow = Custom("ServerCow", "./resources/servercow.svg")
        email = Custom("transip.email (BIJ1)", email_logo)
        mailcow = Custom("MailCow", "./resources/mailcow.svg")
        email_intl = Custom("transip.email (Audre Lorde)", email_logo)
        email_kennis = Custom("transip.email (Clemencia Redmond)", email_logo)
        email_pjo = Custom("webmail.greenhost.nl (Radicaal)", email_logo)
        email >> transip
        email_intl >> transip_intl
        email_kennis >> transip_kennis
        email_pjo >> greenhost_pjo
        mailcow >> servercow
        heroku_flyer = Custom("bij1-data-dashboard.herokuapp.com", heroku)
        heroku_flyer_demo = Custom("bij1-flyer-app.herokuapp.com", heroku)
    
    with Cluster("Web domains"):
        bij1_org = Custom("bij1.org", www)
        bij1_org >> transip
        bij1_org >> email
        bij1_net = Custom("bij1.net", www)
        bij1_net >> greenhost
        radicaal_org = Custom("radicaal.org", www)
        radicaal_org >> greenhost_pjo
        radicaal_org >> email_pjo
        clemenciaredmond_org = Custom("clemenciaredmond.org", www)
        clemenciaredmond_org >> transip_kennis
        clemenciaredmond_org >> email_kennis

    with Cluster("Code repositories"):
        gitlab = Custom("gitlab.com/bij1", gitlab_logo)
        github = Custom("github.com/bij1", github_logo)
        github_data = Custom("github.com/bij1data", github_logo)
        gitea = Custom("code.bij1.org", "./resources/gitea.svg")
        woodpecker = Custom("build.bij1.org", "./resources/woodpecker.png")
        github >> gitlab >> gitea
        gitea >> woodpecker

    with Cluster("Roadmap"):
        trello_campaign = Custom("Trello Campagne Tweede Kamer verkiezingen", trello)
        trello_it = Custom("Trello BIJ1 ICT", trello)
        gitlab_issues = Custom("Gitlab issues", gitlab_logo)
        github_projects = Custom("BIJ1 Data kanban", github_logo)
        email >> trello_campaign
        github_projects >> github_data

    with Cluster("Aanmeldingen Kanban"):
        trello_vrijwilligers = Custom("Trello board NieuweBIJ1ers", trello)
        localize_trello = Python("localize-trello")
        with Cluster("Lokale aanmeldingen"):
            local_trello = Custom("trello by region", trello)
            trello_vrijwilligers >> localize_trello >> local_trello
        trello_campaign >> trello_vrijwilligers

    with Cluster("Nieuwsbrieven"):
        mailchimp_central = Custom("Landelijke MailChimp", mailchimp)
        with Cluster("Lokale nieuwsbrieven"):
            mailchimp_central = Custom("Lokale MailChimps", mailchimp)

    with Cluster("ELK"):

        logstash = Logstash("Logstash")
        beats = Beats("Beats")
        elasticsearch = Elasticsearch("Elasticsearch")
        kibana = Kibana("Kibana")

        logstash >> beats
        logstash >> elasticsearch
        logstash >> kibana

        greenhost >> logstash
        greenhost >> beats
        greenhost >> elasticsearch
        greenhost >> kibana

    with Cluster("Meetings"):
        zoom = Custom("Zoom", "./resources/zoom.svg")
        jitsi = Custom("meet.bij1.org", "./resources/jitsi.svg")
        bbb = Custom("bigbluebutton.bij1.org", "./resources/bigbluebutton.svg")
        bbb >> hetzner
        ingang = Rails("vergadering.bij1.org/ingang/")
        flowmailer = Custom("FlowMailer", "./resources/flowmailer.svg")
        helios = Custom("stemmen.bij1.org", "./resources/helios.png")
        coturn = Custom("turn.bij1.org", "./resources/webrtc.png")
        coturn >> bbb >> ingang >> helios
        ingang >> flowmailer

        jitsi >> greenhost
        ingang >> greenhost
        helios >> greenhost
        coturn >> greenhost

    with Cluster("Websites"):
        proxy = Custom("proxy.bij1.net", nginx)
        bij1_wp = Custom("bij1.org", wordpress)
        staging_wp = Custom("staging-wp.bij1.net", wordpress)
        doemee_wp = Custom("doemee.bij1.org", wordpress)
        doemee_wp >> wpengine
        doemee_wp >> trello_vrijwilligers
        radicaal = Custom("radicaal.bij1.org", "./resources/squarespace.png")
        shop = Custom("shop.bij1.org", "./resources/woocommerce.svg")
        wings_bij1 = Custom("Wings", wings)
        proxy >> bij1_wp

        proxy >> greenhost
        bij1_wp >> greenhost
        staging_wp >> greenhost
        shop >> bij1_wp
        bij1_wp >> wings_bij1 # donations

        with Cluster("Static"):
            static = Custom("static.bij1.net", nginx)
            links = Custom("links.bij1.org", www)
            # actie = Custom("actie.bij1.org", www)
            # iktelookmee = Custom("iktelookmee.bij1.org", www)
            # wijzijn = Custom("wijzijn.bij1.org", www)
            static >> links
            # static >> actie
            # static >> iktelookmee
            # static >> wijzijn

            static >> greenhost

        with Cluster("Afdelingssites"):
            wings_amsterdam = Custom("amsterdam.bij1.org", wings)
            wings_rotterdam = Custom("rotterdam.bij1.org", wings)
            wings_denhaag = Custom("denhaag.bij1.org", wings)
            wings_utrecht = Custom("utrecht.bij1.org", wings)
            wings_hilversum = Custom("hilversum.bij1.org", wings)
            wp_almere = Custom("almerebij1.org", wordpress)
            wp_amsterdam_staging = Custom("amsterdam.wp-staging.bij1.net", wordpress)
            wp_amersfoort_staging = Custom("amersfoort.wp-staging.bij1.net", wordpress)
            wp_almere_staging = Custom("almere.wp-staging.bij1.net", wordpress)
            wp_rotterdam_staging = Custom("rotterdam.wp-staging.bij1.net", wordpress)
            wp_hilversum_staging = Custom("hilversum.wp-staging.bij1.net", wordpress)

    zapier = Custom("Zapier", "./resources/zapier.png")
    doemee_wp >> zapier
    airtable = Custom("Airtable", "./resources/airtable.png")
    zapier >> airtable

    with Cluster("Intranet"):

        # kom = Custom("kom.bij1.org", www)
        # kom >> transip
        # keycloak = Custom("jij.bij1.org", "./resources/keycloak.svg")
        cloud = Nextcloud("cloud.bij1.org")
        cloud >> greenhost
        # kom >> keycloak
        # keycloak >> cloud

        # what shouldn't be our cloud but currently is 😩
        gdrive = Custom("Google Drive", "./resources/google-drive.png")

        mega = Custom("Mega", "./resources/mega.svg")

    with Cluster("CRM"):
        civi = Custom("CiviCRM", "./resources/civicrm.svg")
        bij1algemeen = Php("bij1algemeen")
        bij1rules = Php("bij1rules")
        bij1migratie = Php("bij1migratie")
        bij1_wp >> civi
        airtable >> bij1migratie >> civi
    
    with Cluster("chat"):
        with Cluster("Discord"):
            discord_bij1 = Discord("BIJ1 Discord (unofficial)")
            discord_radicaal = Discord("Radicaal Discord")
            with Cluster("Local Discords"):
                discords_local = Discord("Local BIJ1 Discords")
        fb_group = Facebook("Facebook groep actieve leden")
        with Cluster("Signal"):
            signal = Custom("signal groups (many)", "./resources/signal.png")

    mollie = Custom("Mollie", "./resources/mollie.jpg")
    export_mollie = Nodejs("export-mollie")
    mollie >> export_mollie >> bij1migratie

    terraform = Custom("Terraform", "./resources/terraform.png")
    terraform_cloud = Custom("Terraform Cloud", "./resources/terraform-cloud.png")
    terraform >> terraform_cloud

    canva = Custom("Canva", "./resources/canva.png")

    snelstart = Custom("snelstart", "./resources/snelstart.svg")

    with Cluster("link collections"):
        linktree = Custom("Linktree", "./resources/linktree.png")
        linktree >> local_insta
        linkbee = Custom("links.bij1.org", "./resources/link.png")
        linkbee >> insta
