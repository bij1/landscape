# BIJ1 Landscape

This repo offers a diagram to show the BIJ1 IT landscape and how its components connect.

Note: this diagram is descriptive, not where we'd ideally like to be.
Our ideal landscape will involve, in no particular order:

- getting rid of non-EU vendors
- shifting from commercial services to open-source self-reliance
- more automation between systems
- better [systems reliability](https://sretips.com.br/sre-piramide.jpg)
- better unification of systems, rather than every local branch having to reinvent the wheel in their own accounts
- unified login system to handle authentication and authorization across our systems
- better reproducibility (think documentation, CI/CD, infra-as-code)
- ideally open-sourcing more still than we've managed now

## requirements

- [GraphViz](https://graphviz.gitlab.io/download/)
- [Make](https://www.gnu.org/software/make/)

## usage

```bash
make install
make diagram
```

## diagram

![landscape](./bij1_it_landscape.png)
