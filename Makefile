ACTIVATE =. ./env/bin/activate

# Target to create virtual env. No phony as we want to check whether it exists or not.
venv:
	@python3 -m venv ./env

.PHONY: install
install: venv
	@$(ACTIVATE); pip install --upgrade pip; pip install -r requirements.txt

diagram:
	@$(ACTIVATE); python diagram.py
